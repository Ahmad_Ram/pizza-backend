<?php

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::create(['name' => 'pending']);
        Status::create(['name' => 'unsufficient money']);
        Status::create(['name' => 'delivered']);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\Order;
use App\Models\User;
use App\Models\Pizza;
use App\Models\Role;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = User::where('role_id', Role::where('name', 'customer')->first()->id )->get();
        $n = count($customers);

        $order = Order::create([

            'user_id' => $customers[0]->id
        ]);

        $order->pizzas()->attach(Pizza::where('id','<','6')->get());

        $order = Order::create([

            'user_id' => $customers[0]->id
        ]);

        $order->pizzas()->attach(Pizza::where('id','>','6')->get());

        $order = Order::create([

            'user_id' => $customers[$n-1]->id
        ]);

        $order->pizzas()->attach(Pizza::where('id','<','6')->get());
    }
}

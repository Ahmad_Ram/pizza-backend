<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'firstname' => 'mark',
            'lastname' => 'qelas',
            'email' => 'mark.q@mail.com',
            'password' => bcrypt('secret'),
            'role_id' => Role::where('name', 'customer')->first()->id
        ]);

        User::create([
            'firstname' => 'emma',
            'lastname' => 'bages',
            'email' => 'emma.b@mail.com',
            'password' => bcrypt('secret'),
            'role_id' => Role::where('name', 'customer')->first()->id
        ]);

        User::create([
            'firstname' => 'emp',
            'lastname' => 'one',
            'email' => 'emp.o@mail.com',
            'password' => bcrypt('secret'),
            'role_id' => Role::where('name', 'employee')->first()->id
        ]);
    }
}

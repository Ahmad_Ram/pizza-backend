<?php

use Illuminate\Database\Seeder;
use App\Models\Pizza;

class PizzaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pizza::create([
            'name' => 'Neapolitan Pizza',
            'description' => 'Neapolitan is the original pizza. This delicious pie dates all the way back to 18th century in Naples, Italy. During this time, the poorer citizens of this seaside city frequently purchased food that was cheap and could be eaten quickly. Luckily for them, Neapolitan pizza was affordable and readily available through numerous street vendors.',
            'photo' => 'https://upload.wikimedia.org/wikipedia/commons/a/a3/Eq_it-na_pizza-margherita_sep2005_sml.jpg',
        ]);

        Pizza::create([
            'name' => 'Chicago Pizza',
            'description' => 'Chicago pizza, pizza with a thick crust that had raised edges, similar to a pie, and ingredients in reverse, with slices of mozzarella lining the dough followed by meat, vegetables, and then topped with a can of crushed tomatoes. This original creation led Sewell to create the now famous chain restaurant, Pizzeria Uno.',
            'photo' => 'https://cache-graphicslib.viator.com/graphicslib/page-images/742x525/134832_Chicago_GrabaSliceofChicagoStylePizza_3874.jpg',
        ]);

        Pizza::create([
            'name' => 'Margherita Pizza',
            'description' => 'Pizza Margherita is a typical Neapolitan pizza, made with San Marzano tomatoes, mozzarella cheese, fresh basil, salt and extra-virgin olive oil. Traditionally, it is made with fior di latte rather than buffalo mozzarella',
            'photo' => 'https://img.taste.com.au/Wf8mL7LT/w720-h480-cfill-q80/taste/2016/11/jessica-39581-2.jpeg',
        ]);

        Pizza::create([
            'name' => 'Pepperoni Pizza',
            'description' => 'Pepperoni is an American variety of salami, made from cured pork and beef mixed together and seasoned with paprika or other chili pepper. ... Thinly sliced pepperoni is a popular pizza topping in American pizzerias.',
            'photo' => 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fcdn-image.myrecipes.com%2Fsites%2Fdefault%2Ffiles%2Fstyles%2Fmedium_2x%2Fpublic%2Fimage%2Frecipes%2Fsl%2F03142008%2Fpepperoni-pizza-sl-1599569-x.jpg%3Fitok%3Dga1g5wpq&w=450&c=sc&poi=face&q=85',
        ]);

        Pizza::create([
            'name' => 'Detroit Pizza',
            'description' => 'Detroit-style pizza is a style of pizza developed in Detroit, Michigan. It is a rectangular pizza that has a thick crisp crust and toppings such as pepperoni and mushrooms.',
            'photo' => 'https://www.seriouseats.com/images/2017/02/20170216-detroit-style-pizza-47.jpg',
        ]);

        Pizza::create([
            'name' => 'California Pizza',
            'description' => 'California-style pizza is a style of single-serving pizza that combines New York and Italian thin crust with toppings from the California cuisine cooking style.',
            'photo' => 'https://media-cdn.tripadvisor.com/media/photo-s/10/9f/f0/11/the-golden-hawaiian.jpg',
        ]);

        Pizza::create([
            'name' => 'Marinara Pizza',
            'description' => 'Pizza marinara is a style of Neapolitan pizza in Italian cuisine prepared with plain marinara sauce and seasoned with oregano and garlic. It is very similar to a Pizza Margherita, however it lacks the typical Mozzarella or other cheeses.',
            'photo' => 'https://aussietaste.com.au/wp-content/uploads/2014/04/Seafood-Marinara-Pizza-opt.jpg',
        ]);

        Pizza::create([
            'name' => 'Calzone Pizza',
            'description' => 'A calzone is an Italian oven-baked folded pizza that originated in Naples in the 18th century. A typical calzone is made from salted bread dough, baked in an oven and is stuffed with salami, ham or vegetables, mozzarella, ricotta and Parmesan or pecorino cheese, as well as an egg.',
            'photo' => 'https://s3.envato.com/files/238141816/IMG_8299-4.jpg',
        ]);

        Pizza::create([
            'name' => 'Greek Pizza',
            'description' => 'Greek pizza is a style of pizza crust and its preparation, rather than its toppings. This style is baked in a pan, instead of directly on the bricks of the pizza oven.',
            'photo' => 'https://d1doqjmisr497k.cloudfront.net/-/media/mccormick-us/recipes/grill-mates/g/800/grilled-chicken-greek-pizza.jpg',
        ]);

        Pizza::create([
            'name' => 'Stromboli Pizza',
            'description' => 'Stromboli is a type of turnover filled with various Italian cheeses and cold cuts or vegetables. The dough used is either Italian bread dough or pizza dough.',
            'photo' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8U9OxObI4n9S4_pFlLgmT48-JIrL6osPbKbWuR78m7RRAeOUS',
        ]);
    }
}

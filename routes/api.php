<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// routes for authentication | login | logout | refresh token
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'api\AuthController@login');
    Route::post('logout', 'api\AuthController@logout');
    Route::post('refresh', 'api\AuthController@refresh');
    Route::post('signup', 'api\AuthController@register');
});

Route::group([

    'middleware' => 'api',
    'prefix' => 'orders'

], function ($router) {

    // get all orders of a user
    Route::get('get', 'api\OrderController@index');

    // get a certain order of a user
    Route::get('get/{id}', 'api\OrderController@show');
    Route::post('store', 'api\OrderController@store');
    Route::post('update/{id}', 'api\OrderController@update');
    Route::delete('delete/{id}', 'api\OrderController@delete');
});

Route::group([

    'middleware' => 'api',
    'prefix' => 'pizza'

], function ($router) {

    // get all orders of a user
    Route::get('get', 'api\PizzaController@index');
});

Route::group([

    'middleware' => 'api',
    'prefix' => 'admin/orders'

], function ($router) {

    // get all orders
    Route::get('get', 'api\AdminOrdersController@index');
    Route::get('get/{id}', 'api\AdminOrdersController@show');

    Route::post('change_status', 'api\AdminOrdersController@changeStatus');

    Route::get('get_all_statuses', 'api\AdminOrdersController@getAllStatuses');
});


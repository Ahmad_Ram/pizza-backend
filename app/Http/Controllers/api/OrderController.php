<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Http\Resources\OrderResourceCollection;
use App\Models\Order;
use App\Models\Pizza;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Create a new OrderController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request){

        $user = auth()->user();

        if(!$user->orders){

            return response()->json(['message' => 'no orders were found']);
        }

        return new OrderResourceCollection($user->orders);
    }

    public function show(Request $request, $id){

        $user = auth()->user();

        $order = Order::where('id', $id)->where('user_id', $user->id)->first();

        if(!$order){

            return response()->json(['message' => 'no order for this user with this id was find']);
        }

        return new OrderResource(Order::find($id));
    }

    public function store(Request $request){

        $user = auth()->user();
        $pizza_ids = $request->input('ids');

        $order = Order::create(['user_id' => $user->id]);

        foreach($pizza_ids as $id){

            $pizza = Pizza::find($id);
            $order->pizzas()->save($pizza);
        }

        return response()->json(['message' => 'order was saved successfully']);
    }

    public function update(Request $request, $id){

        $user = auth()->user();

        $order = Order::where('id', $id)->where('user_id', $user->id)->first();

        if(!$order){

            return response()->json(['message' => 'no order for this user with this id was find']);
        }

        $pizza_ids = $request->input('ids');

        foreach($pizza_ids as $id){

            $pizza = Pizza::find($id);
            $order->pizzas()->save($pizza);
        }

        return response()->json(['message' => 'order was updated successfully']);
    }

    public function delete(Request $request, $id){

        $user = auth()->user();

        $order = Order::where('id', $id)->where('user_id', $user->id)->first();

        if(!$order){

            return response()->json(['message' => 'no order for this user with this id was find']);
        }

        // cannot delete orders whose statuses are delivered.
        if($order->status->name == 'delivered'){

            return response()->json(['message' => 'cannot delete orders whose statuses are \"delivered\"']);
        }

        $order->pizzas()->detach($order->pizzas);
        $order->delete();

        return response()->json(['message' => 'order was deleted successfully']);
    }
}

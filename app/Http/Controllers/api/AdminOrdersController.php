<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResourceCollection;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\Status;
use Illuminate\Http\Request;

class AdminOrdersController extends Controller
{
    /**
     * Create a new OrdersController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:api', 'employee']);
    }

    public function index(Request $request){

        $orders = Order::all();

        if(!$orders){

            return response()->json(['message' => 'no orders were found']);
        }

        $data = array();

        foreach($orders as $order){

            array_push($data, new OrderResource($order, true));
        }
        return response()->json($data);
    }

    public function show(Request $request, $id){

        $order = Order::find($id);

        if(!$order){

            return response()->json(['message' => 'no order with this id was find']);
        }

        return new OrderResource($order);
    }

    public function changeStatus(Request $request){

        $order_id = $request->input('order_id');
        $status_id = $request->input('status_id');

        $order = Order::find($order_id);

        if(!$order){

            return response()->json(['message' => 'no order with this id was find']);
        }

        $order->status_id = $status_id;

        if($order->save()){

            return response()->json(['message' => 'order status was changed successfully']);
        }

        return response()->json(['message' => 'Error Occurred, please try againn later'], 500);
    }

    public function getAllStatuses(Request $request){

        return Status::all();
    }
}

<?php

namespace App\Http\Controllers\api;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use Validator;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {

            return response()->json(['error' => 'email or password is incorrect.'], 403);
        }
        return new UserResource(auth()->user(), $token);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        if(!Auth::check()) {
            return response()->json(['error' => 'Unauthorized user.'], 403);
        }

        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return new UserResource(auth()->user(), auth()->refresh());
        return $this->respondWithToken(auth()->refresh());
    }

    public function register(Request $request){

        $rules = [
            'email' => 'required|email|max:50|unique:users',
            'firstname' => 'required|string|max:50',
            'lastname' => 'required|string|max:50',
            'password' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json(['message' => $this->refineValidatorMessages($validator->errors())], 403);
        }

        $user = new User([
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'role_id' => Role::where('name', 'customer')->firstOrFail()->id,
        ]);

        if($user->save()){

            return response()->json(['message' => 'user was created successfully']);
        }

        return response()->json(['message' => 'the system was unable to create the user, please try again', 403]);
    }

    private function refineValidatorMessages($errors){

        $errors = json_decode($errors);
        $result = [];
        $i = 0;
        foreach ($errors as $key => $error){
            $r['key'] = $key;
            $r['message'] = $error[0];
            $result[$i++] = $r;
        }
        return $result;
    }
}
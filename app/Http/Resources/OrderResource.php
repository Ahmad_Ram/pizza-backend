<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\PizzaResourceCollection;
use App\Http\Resources\UserResource;

class OrderResource extends JsonResource
{

    /**
     * @var bool
     * decided if the user associated with the order should be shown or not.
     */
    private $isUserIncluded;
    

    public function __construct($resource, $isUserIncluded = false){

        parent::__construct($resource);

        $this->isUserIncluded = $isUserIncluded;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'status' => $this->status->name,
            'user' => $this->when($this->isUserIncluded, function(){
                 return new UserResource($this->user); 
            }),
            'pizzas' => new PizzaResourceCollection($this->pizzas),
        ];
    }
}

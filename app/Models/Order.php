<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'status_id', 'created_at', 'updated_at'];

    /**
     * Get the role record associated with the user.
     */
    public function user(){

        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function pizzas(){

        return $this->belongsToMany('App\Models\Pizza', 'order_details', 'order_id', 'pizza_id');
    }

    public function status(){

        return $this->belongsTo('App\Models\Status', 'status_id');
    }
}
